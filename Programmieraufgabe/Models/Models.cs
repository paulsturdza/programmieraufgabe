﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Programmieraufgabe.Models
{
    public enum Status
    {
        Frei, Reserviert, Angemeldet, Abgegeben, Bewertet
    }

    public enum Type
    {
        Bachelor, Master
    }

    public class Thesis : IValidatableObject
    {
        [Required]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required]
        [Display(Name ="Titel")]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Beschreibung")]
        public string Description { get; set; }

        [Display(Name = "Bachelor-Thesis")]
        public bool Bachelor { get; set; }

        [Display(Name = "Master-Thesis")]
        public bool Master { get; set; }

        [Display(Name = "Status der Thesis")]
        public Status ThesisStatus { get; set; }

        [Display(Name = "Vollständiger Name des Studenten")]
        public string StudentName { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "Keine gültige eMail-Adresse")]
        [Display(Name = "eMail-Adresse des Studenten")]
        public string StudentEMail { get; set; }

        [Display(Name = "Matrikelnummer des Studenten")]
        public string StudentId { get; set; }

        [Display(Name = "Anmeldedatum")]
        public DateTime? Registration { get; set; }

        [Display(Name = "Abgabedatum")]
        public DateTime? Filing { get; set; }

        [Display(Name = "Geeignet für diese Studiengangsart")]
        public Type? Type { get; set; }

        [Display(Name = "Zusammenfassung")]
        [DataType(DataType.MultilineText)]
        public string Summary { get; set; }

        [Display(Name = "Stärken")]
        [DataType(DataType.MultilineText)]
        public string Strengths { get; set; }

        [Display(Name = "Schwächen")]
        [DataType(DataType.MultilineText)]
        public string Weaknesses { get; set; }

        [Display(Name = "Beurteilung")]
        [DataType(DataType.MultilineText)]
        public string Evaluation { get; set; }
        
        [Display(Name = "Bewertung des Kontents")]
        [Range(1, 5, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (1-5).")]
        public int? ContentVal { get; set; }

        [Display(Name = "Bewertung des Layouts")]
        [Range(1, 5, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (1-5).")]
        public int? LayoutVal { get; set; }

        [Display(Name = "Bewertung der Struktur")]
        [Range(1, 5, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (1-5).")]
        public int? StructureVal { get; set; }

        [Display(Name = "Bewertung des Stils")]
        [Range(1, 5, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (1-5).")]
        public int? StyleVal { get; set; }

        [Display(Name = "Bewertung der Literatur")]
        [Range(1, 5, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (1-5).")]
        public int? LiteratureVal { get; set; }

        [Display(Name = "Bewertung des Schwierigkeitsgrads")]
        [Range(1, 5, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (1-5).")]
        public int? DifficultyVal { get; set; }

        [Display(Name = "Bewertung der Neuheit")]
        [Range(1, 5, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (1-5).")]
        public int? NoveltyVal { get; set; }

        [Display(Name = "Bewertung des Umfangs")]
        [Range(1, 5, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (1-5).")]
        public int? RichnessVal { get; set; }

        [Display(Name = "Gewichtung der Kontentbewertung")]
        [Range(0, 100, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (0-100).")]
        public int? ContentWt { get; set; } = 30;

        [Display(Name = "Gewichtung der Layoutbewertung")]
        [Range(0, 100, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (0-100).")]
        public int? LayoutWt { get; set; } = 15;

        [Display(Name = "Gewichtung der Strukturbewertung")]
        [Range(0, 100, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (0-100).")]
        public int? StructureWt { get; set; } = 10;

        [Display(Name = "Gewichtung der Stilbewertung")]
        [Range(0, 100, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (0-100).")]
        public int? StyleWt { get; set; } = 10;

        [Display(Name = "Gewichtung der Literaturbewertung")]
        [Range(0, 100, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (0-100).")]
        public int? LiteratureWt { get; set; } = 10;

        [Display(Name = "Gewichtung der Schwierigkeitsbewertung")]
        [Range(0, 100, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (0-100).")]
        public int? DifficultyWt { get; set; } = 5;

        [Display(Name = "Gewichtung der Neuheitsbewertung")]
        [Range(0, 100, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (0-100).")]
        public int? NoveltyWt { get; set; } = 10;

        [Display(Name = "Gewichtung der Umfangsbewertung")]
        [Range(0, 100, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (0-100).")]
        public int? RichnessWt { get; set; } = 10;

        [Display(Name = "Note")]
        [Range(1, 5, ErrorMessage = "Die Zahl liegt nicht im gültigen Bereich (1-5).")]
        public int? Grade { get; set; }

        [Display(Name = "Letzte Änderung am Datensatz")]
        public DateTime? LastModified { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext ValContext)
        {
            List<ValidationResult> res = new List<ValidationResult>();

            switch (ThesisStatus)
            {
                case Status.Frei:
                    if (!Bachelor && !Master) res.Add(new ValidationResult("Die Thesis muss als Bachelor- und/oder als Master-Thesis geeignet sein."));
                    break;
                case Status.Reserviert:
                    if (!Bachelor && !Master) res.Add(new ValidationResult("Die Thesis muss als Bachelor- und/oder als Master-Thesis geeignet sein."));
                    if (StudentName == null) res.Add(new ValidationResult("Der Name des Studenten muss bei einer Reservierung angegeben werden."));
                    if (StudentEMail == null) res.Add(new ValidationResult("Die eMail-Adresse des Studenten muss bei einer Reservierung angegeben werden."));
                    break;
                case Status.Angemeldet:
                    if (!Bachelor && !Master) res.Add(new ValidationResult("Die Thesis muss als Bachelor- und/oder als Master-Thesis geeignet sein."));
                    if (StudentName == null) res.Add(new ValidationResult("Der Name des Studenten muss bei einer Anmeldung angegeben werden."));
                    if (StudentEMail == null) res.Add(new ValidationResult("Die eMail-Adresse des Studenten muss bei einer Anmeldung angegeben werden."));
                    if (StudentId == null) res.Add(new ValidationResult("Die Matrikelnummer des Stundenten muss bei einer Anmeldung angegeben werden."));
                    if (Registration == null) res.Add(new ValidationResult("Das Anmeldedatum muss bei einer Anmeldung angegeben werden."));
                    if (Type == null) res.Add(new ValidationResult("Der Typ der Thesis muss bei einer Anmeldung angegeben werden."));
                    break;
                case Status.Abgegeben:
                    if (!Bachelor && !Master) res.Add(new ValidationResult("Die Thesis muss als Bachelor- und/oder als Master-Thesis geeignet sein."));
                    if (StudentName == null) res.Add(new ValidationResult("Der Name des Studenten muss bei einer Abgabe angegeben werden."));
                    if (StudentEMail == null) res.Add(new ValidationResult("Die eMail-Adresse des Studenten muss bei einer Abgabe angegeben werden."));
                    if (StudentId == null) res.Add(new ValidationResult("Die Matrikelnummer des Stundenten muss bei einer Abgabe angegeben werden."));
                    if (Registration == null) res.Add(new ValidationResult("Das Anmeldedatum muss bei einer Abgabe angegeben werden."));
                    if (Type == null) res.Add(new ValidationResult("Der Typ der Thesis muss bei einer Abgabe angegeben werden."));
                    if (Filing == null) res.Add(new ValidationResult("Das Abgabedatum muss bei einer Abgabe angegeben werden."));
                    break;
                case Status.Bewertet:
                    if (!Bachelor && !Master) res.Add(new ValidationResult("Die Thesis muss als Bachelor- und/oder als Master-Thesis geeignet sein."));
                    if (StudentName == null) res.Add(new ValidationResult("Der Name des Studenten muss bei einer Bewertung angegeben werden."));
                    if (StudentEMail == null) res.Add(new ValidationResult("Die eMail-Adresse des Studenten muss bei einer Bewertung angegeben werden."));
                    if (StudentId == null) res.Add(new ValidationResult("Die Matrikelnummer des Stundenten muss bei einer Bewertung angegeben werden."));
                    if (Registration == null) res.Add(new ValidationResult("Das Anmeldedatum muss bei einer Bewertung angegeben werden."));
                    if (Type == null) res.Add(new ValidationResult("Der Typ der Thesis muss bei einer Bewertung angegeben werden."));
                    if (Filing == null) res.Add(new ValidationResult("Das Abgabedatum muss bei einer Bewertung angegeben werden."));
                    if (Summary == null) res.Add(new ValidationResult("Die Zusammenfassung muss bei einer Bewertung angegeben werden."));
                    if (Strengths == null) res.Add(new ValidationResult("Die Stärken müssen bei einer Bewertung angegeben werden."));
                    if (Weaknesses == null) res.Add(new ValidationResult("Die Schwächen müssen bei einer Bewertung angegeben werden."));
                    if (Evaluation == null) res.Add(new ValidationResult("Die Evaluation muss bei einer Bewertung angegeben werden."));
                    if (ContentVal == null) res.Add(new ValidationResult("Die Kontentbewertung muss bei einer Bewertung angegeben werden."));
                    if (LayoutVal == null) res.Add(new ValidationResult("Die Layoutbewertung muss bei einer Bewertung angegeben werden."));
                    if (StructureVal == null) res.Add(new ValidationResult("Die Strukturbewertung muss bei einer Bewertung angegeben werden."));
                    if (StyleVal == null) res.Add(new ValidationResult("Die Stilbewertung muss bei einer Bewertung angegeben werden."));
                    if (LiteratureVal == null) res.Add(new ValidationResult("Die Literaturbewertung muss bei einer Bewertung angegeben werden."));
                    if (DifficultyVal == null) res.Add(new ValidationResult("Die Schwierigkeitsgradbewertung muss bei einer Bewertung angegeben werden."));
                    if (NoveltyVal == null) res.Add(new ValidationResult("Die Neuheitsbewertung muss bei einer Bewertung angegeben werden."));
                    if (RichnessVal == null) res.Add(new ValidationResult("Die Umfangsbewertung muss bei einer Bewertung angegeben werden."));
                    if (ContentWt == null) res.Add(new ValidationResult("Das Gewicht der Kontentbewertung muss bei einer Bewertung angegeben werden"));

                    if (LayoutWt == null) res.Add(new ValidationResult("Das Gewicht der Layoutbewertung muss bei einer Bewertung angegeben werden."));
                    if (StructureWt == null) res.Add(new ValidationResult("Das Gewicht der Strukturbewertung muss bei einer Bewertung angegeben werden."));
                    if (StyleWt == null) res.Add(new ValidationResult("Das Gewicht der Stilbewertung muss bei einer Bewertung angegeben werden."));
                    if (LiteratureWt == null) res.Add(new ValidationResult("Das Gewicht der Literaturbewertung muss bei einer Bewertung angegeben werden."));
                    if (DifficultyWt == null) res.Add(new ValidationResult("Das Gewicht der Schwierigkeitsgradbewertung muss bei einer Bewertung angegeben werden."));
                    if (NoveltyWt == null) res.Add(new ValidationResult("Das Gewicht der Neuheitsbewertung muss bei einer Bewertung angegeben werden."));
                    if (RichnessWt == null) res.Add(new ValidationResult("Das Gewicht der Umfangsbewertung muss bei einer Bewertung angegeben werden."));
                    if (Grade == null) res.Add(new ValidationResult("Die Note muss bei einer Bewertung angegeben werden."));
                    if (ContentWt + LayoutWt + StructureWt + StyleWt + LiteratureWt + DifficultyWt + NoveltyWt + RichnessWt != 100) res.Add(new ValidationResult("Die Summe der Gewichte ist nicht korrekt!"));
                    break;
            }
            return res;
        }

    }

    public class Programme
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}