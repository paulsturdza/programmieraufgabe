﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Programmieraufgabe.Startup))]
namespace Programmieraufgabe
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
